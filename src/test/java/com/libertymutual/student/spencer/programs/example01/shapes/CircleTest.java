/*
 * Copyright (c) 2019, Liberty Mutual Group
 *
 * Created on Aug 27, 2019
 */
package com.libertymutual.student.spencer.programs.example01.shapes;

import org.junit.Test;
import org.junit.Before;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CircleTest {

  Circle circleForTest;

  @Before
  public void setup(){
    circleForTest = new Circle(10, Color.red);
  }

  @Test
  public void testGetArea() {
    BigDecimal area = circleForTest.getArea();
    BigDecimal expectedAnswer = new BigDecimal(314);
    assertEquals("Verify that the area is correct:", expectedAnswer, area);
  }

  @Test
  public void testColor(){
    assertEquals("Verify the colors", Color.red, circleForTest.getColor());
  }
}