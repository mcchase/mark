/*
 * Copyright (c) 2019, Liberty Mutual Group
 *
 * Created on Aug 27, 2019
 */
package com.libertymutual.student.spencer.programs.example01.shapes;

import java.awt.*;
import java.math.BigDecimal;

public class Square extends Shape {
  private int length;

  public Square(int length, Color color) {
    super(color);
    this.length = length;
  }

  @Override
  public BigDecimal getArea(){
    double area = length * length;
    return new BigDecimal(area);
  }
}
