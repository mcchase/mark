package com.libertymutual.student.spencer.programs.example01;

import com.libertymutual.student.spencer.programs.example01.shapes.Circle;
import com.libertymutual.student.spencer.programs.example01.shapes.Square;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;
import java.math.BigDecimal;

public class App {

    private final static Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {

        logger.info(App.class + " starting ");
        int radius = 10;
        Circle circle = new Circle(radius, Color.PINK);
        BigDecimal area = circle.getArea();
        System.out.println(area);

        int length = 100;
        Square square = new Square(length, Color.RED);
        System.out.println(square.getArea());
    }
}