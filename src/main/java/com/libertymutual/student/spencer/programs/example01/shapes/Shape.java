package com.libertymutual.student.spencer.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

abstract public class Shape {
    private final static Logger logger = LogManager.getLogger(Shape.class);

    private Color color;

    public Shape(Color color) {
        logger.info("Creating shape base class");
        this.color = color;
    }

    public abstract BigDecimal getArea();

    public Color getColor(){
        return color;
    }

}